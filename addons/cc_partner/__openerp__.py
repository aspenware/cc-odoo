# -*- coding: utf-8 -*-

{
    'name': 'Caption Colorado Partner',
    'version': '0.1',
    'category': 'Tools',
    'description': "Caption Colorado Partner Extensions",
    'author': 'Aspenware',
    'website': 'http://www.aspenware.com',
    'summary': 'Caption Colorado Partner',
    'depends': ['sale', 'cc_servicetype'],
    'data': [
        'security/cc_partner_security.xml',
        'security/ir.model.access.csv',
        'cc_partner_view.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'images': ['images/aspenware.png','images/aspenware.png','images/aspenware.png'],
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
