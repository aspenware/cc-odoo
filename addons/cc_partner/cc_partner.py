# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import osv, fields
from openerp import models
from openerp.tools.translate import _


class cc_partner(osv.osv):
    _inherit = 'res.partner'
    _name = 'res.partner'
    _description = 'Customer extensions.'

    _columns = {
        'connection': fields.char('Connection', required=False, translate=False),
        'vpn_required': fields.boolean('VPN Required', required=False, translate=False),
        'audio': fields.char('Audio', required=False, translate=False),
        'sip_required': fields.boolean('SIP Required', required=False, translate=False),
        'caption_placement': fields.char('Caption Placement', required=False, translate=False),
        'dial_up': fields.boolean('Dial-up', required=False, translate=False),
        'ip': fields.boolean('IP', required=False, translate=False),
        'spanish': fields.boolean('Spanish', required=False, translate=False),
        'international_flag': fields.boolean('International Flag', required=False, translate=False),
        'postable': fields.boolean('Postable', required=False, translate=False),
        'pending_event_status': fields.boolean('Pending Event Status', required=False, translate=False),
        'team': fields.boolean('Team', required=False, translate=False),
        'check_in': fields.boolean('Check-In', required=False, translate=False),
        'accounting_hold_flag': fields.boolean('Accounting Hold Flag', required=False, translate=False),
        'emergencyapproval': fields.boolean('Emergency Approval', required=False, translate=False),
        'extensionapproval': fields.boolean('Extension Approval', required=False, translate=False),
        'scriptphonelocal': fields.char('Local', required=False, translate=False),
        'scriptphonetollfree': fields.char('Toll Free', required=False, translate=False),
        'scriptphonelocalalt': fields.char('Local Alt', required=False, translate=False),
        'scriptphonetollfreealt': fields.char('Toll Free Alt', required=False, translate=False),
        'scriptemail': fields.char('EMail', required=False, translate=False),
        'scriptemaillogin': fields.char('User', required=False, translate=False),
        'scriptemailpassword': fields.char('Password', required=False, translate=False),
        'audiobackuptollfree': fields.char('Audio Backup TF', required=False, translate=False),
        'phonenewsroom1local': fields.char('Local 1', required=False, translate=False),
        'phonenewsroom2local': fields.char('Local 2', required=False, translate=False),
        'phonenewsroom1toll': fields.char('Toll Free 1', required=False, translate=False),
        'phonenewsroom2toll': fields.char('Toll Free 2', required=False, translate=False),
        'emergencycontact': fields.char('Emergency Contact', required=False, translate=False),
        'taperate6013rteditscroll': fields.char('TapeRate6013RTEditScroll', required=False, translate=False),
        'abbreviation': fields.char('Abbreviation', required=False, translate=False),
        'newsroominternetaccess1usr': fields.char('NR User 1', required=False, translate=False),
        'newsroominternetaccess1pwd': fields.char('NR Password 1', required=False, translate=False),
        'newsroominternetaccess1web': fields.char('NR Web 1', required=False, translate=False),
        'newsroominternetaccess2usr': fields.char('NR User 2', required=False, translate=False),
        'newsroominternetaccess2pwd': fields.char('NR Password 2', required=False, translate=False),
        'newsroominternetaccess2web': fields.char('NR Web 2', required=False, translate=False),
        'street3': fields.char('Street3'),
        'email2': fields.char('Email2'),
        'affiliate_id': fields.many2one('cc.affiliate', 'Affiliate'),
        'ownergroup_id': fields.many2one('cc.ownergroup', 'Owner Group'),
        'risklevel_id': fields.many2one('cc.risklevel', 'Risk Level'),
        'customertype_id': fields.many2one('cc.customertype', 'Customer Type'),
        'contacttype_id': fields.many2one('cc.contacttype', 'Contact Type'),
        'customercredential_ids': fields.one2many('cc.customercredential', "partner_id", "Credentials"),
    }
    _defaults = {
      'is_company' : True,
    }


class cc_affiliate(osv.osv):

    _name = 'cc.affiliate'
    _description = 'Customer affiliate.'

    _columns = {
        'affiliate_ids': fields.one2many('res.partner', 'affiliate_id', 'Customers'),
        'name': fields.char('Name', required=True, translate=False),
        'legacy_id': fields.char('Legacy Id'),
    }

class cc_ownergroup(osv.osv):

    _name = 'cc.ownergroup'
    _description = 'Customer owner group.'

    _columns = {
        'ownergroup_ids': fields.one2many('res.partner', 'ownergroup_id', 'Customers'),
        'name': fields.char('Name', required=True, translate=False),
        'legacy_id': fields.char('Legacy Id'),
    }

class cc_risklevel(osv.osv):

    _name = 'cc.risklevel'
    _description = 'Customer Risk Level'

    _columns = {
        'risklevel_ids': fields.one2many('res.partner', 'risklevel_id', 'Customers'),
        'name': fields.char('Name', required=True, translate=False),
        'level': fields.integer('Level', required=True, translate=False),
        'legacy_id': fields.char('Legacy Id'),
    }

class cc_customertype(osv.osv):

    _name = 'cc.customertype'
    _description = 'Customer customertype.'

    _columns = {
        'customertype_ids': fields.one2many('res.partner', 'customertype_id', 'Customers'),
        'name': fields.char('Name', required=True, translate=False),
        'legacy_id': fields.char('Legacy Id'),
    }

class cc_contacttype(osv.osv):

    _name = 'cc.contacttype'
    _description = 'Customer Contact Type.'

    _columns = {
        'contacttype_ids': fields.one2many('res.partner', 'contacttype_id', 'Contacts'),
        'name': fields.char('Name', required=True, translate=False),
        'legacy_id': fields.char('Legacy Id'),
    }

class cc_customercredential(osv.osv):

    _name = 'cc.customercredential'
    _description = 'Customer Credential.'

    _columns = {
        'partner_id': fields.many2one('res.partner', 'Customers'),
        'username': fields.char('User Name', required=True, translate=False),
        'password': fields.char('Password',  required=False, translate=False),
        'isactive': fields.boolean('Is Active', required=True, translate=False),
        'credentialtype_id': fields.many2one('cc.customercredentialtype', 'Credential Type'),
        'legacy_id': fields.char('Legacy Id'),
    }

class cc_customercredential(osv.osv):

    _name = 'cc.customercredentialtype'
    _description = 'Customer Credential Type'

    _columns = {
        'name': fields.char('Credential Type', required=True),
    }




class cc_customer_service_rating(osv.osv):
    _name = 'cc.customer.service.rating'
    _description = "CC Customer Service Rating"
    _order = 'sequence'


    _columns = {
        'display_name': fields.char('Display Name'),
        'partner_id': fields.many2one('res.partner', 'Customer'),
        'service_group_id': fields.many2one('cc.servicetype.servicegroup', 'Service Group'),
        'cc_servicetype_rating': fields.many2one('cc.servicetype.customerserviceratingstype', 'Rating Name', domain=[('service_group_id.name', '=', 'service_group_id.name')]),
        'legacy_id': fields.char('Legacy Id'),
        'sequence': fields.integer('Sequence'),
    }

class cc_customer_note(osv.osv):
    _name = 'cc.customer.note'
    _description = "CC Customer Note"
    _order = 'sequence'
    _columns = {
        'name': fields.char('Name'),
        'legacy_id': fields.char('Legacy Id'),
        'partner_id': fields.many2one('res.partner', 'Customer'),
        'note': fields.text('Note'),
        'cc_customer_tag_ids': fields.many2many('cc.customer.note.tags', 'cc_customer_note_tag_rel', 'note_id', 'tag_id', 'Types'),
        'sequence': fields.integer('Sequence'),
    }

class cc_customer_note_tags(osv.osv):
    _name = 'cc.customer.note.tags'
    _description = 'CC Customer Note Types'
    _order = 'sequence'

    _columns = {
        'name': fields.char('Name'),
        'sequence': fields.integer('Sequence'),
    }

def onchange_facility_id(self, cr, uid, ids, facility_id, context=None):
    context = context or {}
    result = {}

    partner_id = context["default_partner_id"]
    partner_obj = self.pool.get('res.partner')

    if facility_id:
        facility = self.pool.get('cc.facility').browse(cr, uid, facility_id, context=context)
        result['facility_name'] = facility.name
        #result['facility_code'] = facility.code

    if partner_id and facility_id:
        part_obj = partner_obj.browse(cr, uid, partner_id, context=context)
        facility_obj = self.pool.get('cc.facility').browse(cr, uid, facility_id, context=context)

        partnerName = part_obj.name or ''
        facilityName = facility_obj.name or ''
        result['name'] =  partnerName + " - " + facilityName

    return {'value': result}

class res_partner(osv.osv):
    _inherit = 'res.partner'

    _columns = {
        'legacy_id': fields.char('Legacy Id'),
        'cc_servicetype_customer_rating_ids': fields.one2many('cc.customer.service.rating', 'partner_id', 'Customer Ratings'),
        'cc_customer_note_ids': fields.one2many('cc.customer.note', 'partner_id', 'Customer Notes'),
    }


class res_partner(models.Model):
    _inherit = 'res.partner'

