# -*- coding: utf-8 -*-

{
    'name': 'Caption Colorado Resource',
    'version': '0.1',
    'category': 'Tools',
    'description': """
This module supports managing employee
=================================================================

Use for managing employee

""",
    'author': 'Aspenware',
    'category': 'Caption Colorado',
    'website': 'http://www.aspenware.com',
    'summary': 'Caption Colorado Resource',
    'depends': [
        'hr',
        'hr_holidays',
        'resource',
        'cc_partner',
        'cc_servicetype'
    ],
    'data': [
        'security/cc_resource_security.xml',
        'security/ir.model.access.csv',
        'cc_resource_view.xml',
        'cc_resource_data.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
