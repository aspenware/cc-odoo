# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import osv, fields
from openerp.tools.translate import _

from openerp import tools

from datetime import datetime, timedelta
from openerp import SUPERUSER_ID, api

import pytz
from openerp import SUPERUSER_ID

class cc_resource(osv.osv):
    _name = 'cc.resource'
    _description = 'CC Resource'
    _columns = {}


class cc_employee_prefhours(osv.osv):
    _name = 'cc.employee.prefhours'
    _description = "CC Employee Preferred Hours"
    _order = 'sequence'

    _columns = {
        'name': fields.char('Name'),
        'employee_id': fields.many2one('hr.employee', 'Employee'),
        'cc_servicetype_rating': fields.many2one('cc.servicetype.employeeserviceratingstype', 'Name'),
        'legacy_id': fields.char('Legacy Id'),
        'sequence': fields.integer('Sequence'),
    }


class cc_employee_service_rating(osv.osv):
    _name = 'cc.employee.service.rating'
    _description = "CC Employee Service Rating"
    _order = 'sequence'

    _columns = {
        'display_name': fields.char('Display Name'),
        'employee_id': fields.many2one('hr.employee', 'Employee'),
        'service_group_id': fields.many2one('cc.servicetype.servicegroup', 'Service Group'),
        'cc_servicetype_rating': fields.many2one('cc.servicetype.employeeserviceratingstype', 'Rating Name', domain=[('service_group_id.name', '=', 'service_group_id.name')]),
        'max_continuous_hrs':fields.float("Max Continuous Hours"),
        'pre_schedule': fields.boolean("Pre-Schedule"),
        'legacy_id': fields.char('Legacy Id'),
        'sequence': fields.integer('Sequence'),
    }


class cc_employee_equipment(osv.osv):
    """ Audio """
    _name = 'cc.employee.equipment'
    _description = "Equipment"
    _order = 'sequence'

    _columns = {
        'name': fields.char('Name'),
        'legacy_id': fields.char('Legacy Id'),
        'employee_id': fields.many2one('hr.employee', 'Employee'),
        'computer_name': fields.char('Computer Name'),
        'system_guid': fields.char('System GUID'),
        'bicc_version': fields.char('BiCC Version'),
        'os_version': fields.char('OS Version'),
        'memory_physical': fields.float('Memory Physical'),
        'cpu_brand': fields.char('CPU Brand'),
        'screen_resolution': fields.char('Screen Resolution'),
        'security_key_sn': fields.char('Security Key SN'),
        'security_key_sn_as_hex': fields.char('Security Key SN as Hex'),
        'internet_connection_type': fields.char('Internet Connection Type'),
        'internet_connection_desc': fields.char('Internet Connection Desc'),
        'vpn_client': fields.char('VPN Client'),
        'vpn_running': fields.boolean('VPN Running'),
        'caption_sw': fields.char('Caption SW'),
        'caption_sw_version': fields.char('Caption SW Version'),
        'note': fields.text('Notes'),
        'sequence': fields.integer('Sequence'),
    }
    _defaults = {
    }


class cc_employee_note(osv.osv):
    _name = 'cc.employee.note'
    _description = "CC Employee Note"
    _order = 'sequence'
    _columns = {
        'name': fields.char('Name'),
        'legacy_id': fields.char('Legacy Id'),
        'employee_id': fields.many2one('hr.employee', 'Employee'),
        'note': fields.text('Note'),
        'cc_employee_tag_ids': fields.many2many('cc.employee.note.tags', 'cc_employee_note_tag_rel', 'note_id', 'tag_id', 'Tags'),
        'sequence': fields.integer('Sequence'),
    }


class cc_employee_note_tags(osv.osv):
    _name = 'cc.employee.note.tags'
    _description = 'Employee Note Type Tags'
    _order = 'sequence'

    _columns = {
        'name': fields.char('Name'),
        'sequence': fields.integer('Sequence'),
    }

class cc_captioner_types(osv.osv):
    _name = 'cc.captioner.types'
    _description = 'Captioner Types'
    _order = 'sequence'

    _columns = {
        'name': fields.char('Name'),
        'sequence': fields.integer('Sequence'),
    }

class resource_calendar(osv.osv):
    _inherit = 'resource.calendar'

    def onchange_date_range(self, cr, uid, ids, start_date, end_date, context=None):

        name = 'preferred hours: ' + str(start_date) + ' - ' + str(end_date)
        vals = {'name': name}

        return {'value': vals}


    def action_view(self, cr, uid, ids, context=None):

        assert len(ids) == 1, 'This option should only be used for a single id at a time'

        id = ids[0];
        resCal = self.browse(cr, uid, id, context=context)

        context['resource_calendary_id'] = id

        try:
            dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'cc_resource', 'view_resource_calendar_attendance_extension_tree')
        except ValueError, e:
            view_id = False

        result = {
            'name': _('Attendance'),
            'view_type': 'form',
            'view_mode': 'tree',
            'view_id': view_id,
            'res_model': 'resource.calendar.attendance',
            'type': 'ir.actions.act_window',
            'domain': [('calendar_id','=',id)],
            'target': 'current',
            'context': context
        }
        return result


    def _get_working_hour_ids(self, cr, uid, ids, field_name, arg, context=None):

        result = {}
        if not ids: return result

        for calendar_id in ids:
            result.setdefault(calendar_id, [])

            att_obj = self.pool.get("resource.calendar.attendance")
            attendance_ids = att_obj.search(cr, uid, [('calendar_id', '=', calendar_id), ('type', '=', '0')], context=context)
            for att_id in attendance_ids:
                result[calendar_id].append(att_id)
        return result

    _columns = {
        'legacy_id': fields.char('Legacy Id'),
        'employee_id': fields.many2one('hr.employee', 'Employee'),
        'start_date': fields.date('Start Date'),
        'end_date': fields.date('End Date'),
        'working_hour_ids': fields.one2many('resource.calendar.attendance', 'calendar_id', 'Working Hours', domain=[('type','=','0')], copy=True),
        'blocked_hour_ids': fields.one2many('resource.calendar.attendance', 'calendar_id', 'Blocked Hours', domain=[('type','=','1')], copy=True),
        'days_off_ids': fields.one2many('resource.calendar.attendance', 'calendar_id', 'Days Off', domain=[('type','=','2')], copy=True),
        #'working_hour_ids': fields.function(_get_working_hour_ids, type='one2many', obj='resource.calendar.attendance', string="Working Hours"),
    }

class resource_calendar_attendance(osv.osv):
    _inherit = 'resource.calendar.attendance'

    def read(self, cr, user, ids, fields=None, context=None, load='_classic_read'):
        return super(resource_calendar_attendance, self).read(cr, user, ids, fields, context, load)

    def search(self, cr, uid, args, offset=0, limit=None, order=None,
            context=None, count=False):
        if context is None:
            context = {}

        if context.get('active_id',False):
            args += [('calendar_id','=',context.get('active_id'))]


        return super(resource_calendar_attendance, self).search(cr, uid, args, offset, limit, order, context, count)

    def onchange_date(self, cr, uid, ids, start, end, calendar_id, context=None):

        obj_cal = self.pool.get('resource.calendar')
        cal = obj_cal.browse(cr, uid, calendar_id, context=context)

        tz = self.get_my_tz(cr, uid)
        result = self.get_wk_day_hr(tz, str(start))

        hour_from = result['hr']
        dayofweek = str(result['dys'])
        weeknumber = str(int(result['wk']) + int(1))
        name = 'attendance: ' + weeknumber + ' - ' + dayofweek

        result = self.get_wk_day_hr(tz, str(end))
        hour_to = result['hr']

        vals = { 'name': name, 'hour_from': hour_from, 'hour_to': hour_to, 'dayofweek': dayofweek, 'weeknumber': weeknumber }
        return {'value': vals}


    def onchange_change(self, cr, uid, ids, weeknumber, dayofweek, type, context=None):

        name = 'attendance: ' + str(weeknumber) + ' - ' + str(ids)
        vals = {'name': name}
        if type == '2':
            vals = {'name': name, 'hour_from': 0.01, 'hour_to': 23.99 }

        return {'value': vals}


    def get_my_tz(self, cr, uid):

        '''
        tz = 'MST7MDT'
        if att != False and att.calendar_id != False and att.calendar_id.employee_id != False:
            tz = att.calendar_id.employee_id.tz
        tz = pytz.timezone(tz) or pytz.utc
        '''

        user_pool = self.pool.get('res.users')
        user = user_pool.browse(cr, uid, uid)

        tz = 'MST7MDT'
        if user != False and user.partner_id != False and user.partner_id.tz != False:
            tz = user.partner_id.tz

        tz = pytz.timezone(tz) or pytz.utc

        return tz

    def last_sunday(self):
        today = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)

        days_of_week = ['monday','tuesday','wednesday',
                            'thursday','friday','saturday','sunday']
        target_day = days_of_week.index('sunday') + 1

        print ' last_sunday: ' + str(today.isoweekday()) + ': day of week: ' + str(target_day)

        delta_day = target_day - today.isoweekday()
        if delta_day > 0: delta_day -= 7 # go back 7 days

        return today + timedelta(days=delta_day)

    def get_local_time(self, tz, wk, dayofwk, hr):

        hrs = long(hr)


        hrsRemander = hr - float(hrs)
        min = long( hrsRemander * 60.0)
        sec = long((hr - hrs - min/60.0) * 3600.0)

        sn = self.last_sunday()

        # set to employee local timezone
        sunday = tz.localize(sn)

        dys = (long(wk) - long(1))*long(7) + long(dayofwk)
        attDt = sunday + timedelta(days=dys)

        attDt = attDt.replace(hour=hrs, minute=min, second=sec)

        # convert to gmt time zone
        attDt = attDt.astimezone(pytz.utc)  # Convert to UTC

        return attDt



    def _get_start(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        for id in ids:
            att = self.browse(cr, uid, id, context=context)

            tz = self.get_my_tz(cr, uid)

            if att.type == '2':
                att.hour_from = 0.01

            st = self.get_local_time(tz, att.weeknumber, att.dayofweek, att.hour_from)

            res[id] = st

        return res

    def _get_end(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        for id in ids:
            att = self.browse(cr, uid, id, context=context)

            tz = self.get_my_tz(cr, uid)

            if att.type == '2':
                att.hour_to = 23.99

            st = self.get_local_time(tz, att.weeknumber, att.dayofweek, att.hour_to)

            res[id] = st
        return res

    def get_wk_day_hr(self, tz, field_value):

        fieldDt = datetime.strptime(field_value, "%Y-%m-%d %H:%M:%S")
        fieldDtTz = pytz.utc.localize(fieldDt)
        fieldDtTz = fieldDtTz.astimezone(tz)

        hrs = fieldDtTz.hour
        min = fieldDtTz.minute
        sec = fieldDtTz.second

        sunday = self.last_sunday()
        sday = tz.localize(sunday)

        total_days = (fieldDtTz - sday).days

        wk = total_days / long(7)
        dys = total_days - wk*long(7)

        hr = float(hrs) + float(min)/60.0 + float(sec)/3600.0

        return { 'wk':wk, 'dys':dys, 'hr':hr }

    def _set_start(self, cr, uid, id, field_name, field_value, arg, context=None):


        att = self.browse(cr, uid, id, context=context)

        tz = self.get_my_tz(cr, uid)


        result = self.get_wk_day_hr(tz, field_value)

        att = self.browse(cr, uid, id, context=context)
        att.hour_from = result['hr']

        att.dayofweek = str(result['dys'])
        att.weeknumber = str(int(result['wk']) + int(1))
        att.name = 'attendance: ' + att.weeknumber + ' - ' + att.dayofweek

        if att.type == '1':
            att.name = 'block: ' + att.weeknumber + ' - ' + att.dayofweek

        if att.type == '2':
            att.hour_from = 0.01
            att.name = 'day-off: ' + att.weeknumber + ' - ' + att.dayofweek

        res={}
        return res

    def _set_end(self, cr, uid, id, field_name, field_value, arg, context=None):

        att = self.browse(cr, uid, id, context=context)

        tz = self.get_my_tz(cr, uid)

        result = self.get_wk_day_hr(tz, field_value)

        att = self.browse(cr, uid, id, context=context)

        att.hour_to = result['hr']
        if att.type == '2':
            att.hour_to = 23.99

        att.dayofweek = str(result['dys'])
        att.weeknumber = str(int(result['wk']) + int(1))

        att.name = 'attendance: ' + att.weeknumber + ' - ' + att.dayofweek

        if att.type == '1':
            att.name = 'block: ' + att.weeknumber + ' - ' + att.dayofweek

        if att.type == '2':
            att.hour_from = 0.01
            att.name = 'day-off: ' + att.weeknumber + ' - ' + att.dayofweek

        res={}
        return res

    def _get_color_name(self, cr, uid, ids, field_name, arg, context=None):

        res = {}

        for att_id in ids:

            color = 'red'
            att_obj = self.pool.get("resource.calendar.attendance")
            atts = att_obj.browse(cr, uid, [att_id], context=context)

            for att in atts:

                if (att.type == '0'):
                    color = 'green'

                if (att.type == '1'):
                    color = 'yellow'

                if (att.type == '2'):
                    color = 'pink'

                res[att_id] = color

        return res


    _columns = {
        'legacy_id': fields.char('Legacy Id'),
        'color_name': fields.function(_get_color_name, type='text', string="Color"),
        'date_start': fields.function(_get_start, fnct_inv=_set_start, type='datetime',  string="Start"),
        'date_end': fields.function(_get_end, fnct_inv=_set_end, type='datetime',  string="End"),
        'weeknumber': fields.selection([('1','Week 1'),('2','Week 2')], 'Week Number', required=False, select=True),
        'dayofweek': fields.selection([('0','Sunday'),('1','Monday'),('2','Tuesday'),('3','Wednesday'),('4','Thursday'),('5','Friday'),('6','Saturday')], 'Day of Week', required=True, select=True),
        'type': fields.selection([('0','Work'),('1','Block'),('2','Off')], 'Type', required=True, select=True),
    }

    _order = 'weeknumber, dayofweek, hour_from'

    _defaults = {
        'weeknumber' : '1',
        'dayofweek' : '0'
    }




class cc_hr_job_type(osv.osv):
    _name = 'cc.hr.job.type'
    _columns = {
        'legacy_id': fields.char('Legacy Id'),
        'name': fields.char('Job Type Name'),
    }


class cc_employee_operating_entity(osv.osv):
    _name = 'cc.employee.operating.entity'
    _columns = {
        'legacy_id': fields.char('Legacy Id'),
        'name': fields.char('Job Type Name'),
    }


class cc_employee_status(osv.osv):
    _name = 'cc.employee.status'
    _columns = {
        'legacy_id': fields.char('Legacy Id'),
        'name': fields.char('Job Type Name'),
    }


class cc_employee_contract(osv.osv):
    _name = 'cc.employee.contract'
    _columns = {
        'legacy_id': fields.char('Legacy Id'),
        'name': fields.char('Job Type Name'),
    }


def _tz_get(self, cr, uid, context):
    # put POSIX 'Etc/*' entries at the end to avoid confusing users - see bug 1086728
    return [(tz,tz) for tz in sorted(pytz.all_timezones, key=lambda tz: tz if not tz.startswith('Etc/') else '_')]

class hr_holidays(osv.osv):
    _inherit = 'hr.holidays'


    def _check_captioner_dates(self, cr, uid, ids, context=None):

        for holiday in self.browse(cr, uid, ids, context=context):

            today = datetime.today();

            leaveType = holiday.holiday_status_id.name
            isCaptioner = False
            isVT = False

            if leaveType == 'Time Off':

                frm = datetime.strptime(holiday.date_from, tools.DEFAULT_SERVER_DATETIME_FORMAT)
                to = datetime.strptime(holiday.date_to, tools.DEFAULT_SERVER_DATETIME_FORMAT)


                # get captioner type from job types
                for ty in holiday.employee_id.job_type_ids:
                    if ty.name == 'Captioner':
                        isCaptioner = True
                    if ty.name == 'VTCaptioner':
                        isVT = True

                # Captioner constraints
                if isVT == True:
                    enrollDate = today + timedelta(14)
                    if enrollDate.date() > frm.date():
                        raise Warning(_('Must be two weeks from today'))
                        return False;
                else:
                    if isCaptioner == True:
                        if today.day > 14:
                            raise Warning(_('Enrolment for leave is over for this month'))
                            return False;
                        if frm.month <= today.month:
                            raise Warning(_('Enrolment only good for next month'))
                            return False;


        return True

    _constraints = [
        (_check_captioner_dates, ' captioner enrollment problem ', ['date_from','date_to']),
    ]

    _columns = {
        'legacy_id': fields.char('Legacy Id'),
    }

class hr_employee(osv.osv):
    _inherit = 'hr.employee'
    _columns = {
        'legacy_id': fields.char('Legacy Id'),
        'base_partner_id': fields.many2one('res.partner', 'Partner'),
        'cc_servicetype_employee_rating_ids': fields.one2many('cc.employee.service.rating', 'employee_id', 'Resource Ratings'),
        'cc_employee_note_ids': fields.one2many('cc.employee.note', 'employee_id', 'Employee Notes'),
        'schedule_ids': fields.one2many('resource.calendar', 'employee_id', 'Preferred Hours'),
        'operating_entity_id': fields.many2one('cc.employee.operating.entity', "Operating Entity"),
        'employee_status_id': fields.many2one("cc.employee.status", "Employee Status"),
        'contract_type_id': fields.many2one("cc.employee.contract", "Contract Type"),
        'captioner_type_id': fields.many2one("cc.captioner.types", "Captioner Type"),
        'equipment_ids': fields.one2many('cc.employee.equipment', 'employee_id', 'Equipment'),
        'job_type_ids': fields.many2many('cc.hr.job.type', 'cc_hr_job_type_rel', 'employee_id', 'job_type_id', 'Job Type'),
        'mentor_id': fields.many2one('hr.employee', 'Mentor'),
        'tz': fields.selection(_tz_get,  'Timezone', size=64,
            help="The partner's timezone, used to output proper date and time values inside printed reports. "
                 "It is important to set a value for this field. You should use the same timezone "
                 "that is otherwise used to pick and render date and time values: your computer's timezone."),
        'partner_id': fields.many2one('res.partner', "Customer"),
        'home_fax_number': fields.char("Home Fax Number"),
        'pager_number': fields.char("Pager Number"),
        'emergency_number': fields.char('Emergency Number'),
        'position': fields.char("Position"),
        'home_portal': fields.char("Home Portal"),
        'hire_date': fields.date("Hire Date"),
        'termination_date': fields.date("Termination Date"),
        'is_captioner': fields.boolean("Is Captioner"),
        'is_us_citizen': fields.boolean("Is US Citizen"),
        'max_hours': fields.float("Max Hours"),
        'bicc': fields.boolean("BiCC"),
        'cap_rating': fields.integer("Captioner Rating"),
        'pref_hours_rate': fields.char("Pref Hours Rate"),
        'pref_hours_exempt': fields.boolean("Pref Hours Exempt"),
        'cap_max_hours': fields.float("Caption Max Hours"),
        'pref_hours_per_month': fields.float("Desired Hours per Month"),
        'daily_max_hours': fields.float("Daily Max Hours"),
        'pref_hours_notes': fields.text("Pref Hours Notes"),
        'emergency_hours_notes': fields.text("Emergency Hours Notes"),
        'alt_im': fields.char("AltIM"),
        'alt_home_phone': fields.char("Alternate Home Phone"),
        'username': fields.char("User Name"),
        'enable_sip': fields.boolean("Enable SIP"),
        'allow_gives': fields.boolean("Allow Gives"),
        'min_on_call_hours': fields.float("Min On Call Hours"),
        'min_self_aar': fields.float("Min Self AAR"),
        'sip_username': fields.char("SIP Username"),
        'sip_password': fields.char("SIP Password"),
        'sip_proxy': fields.char("SIP Proxy"),
        'ip_only': fields.boolean("IP Only"),
        'dial_up': fields.boolean("Dial Up"),
        'vpn_required': fields.boolean("VPN Required"),
        'sip': fields.boolean("SIP Required"),

        'cap_req_note': fields.char("Captioner Req Note"),
    }

