# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import osv, fields
from openerp import models
from openerp.tools.translate import _


class cc_facility(osv.osv):
    """ Note """
    _name = 'cc.facility'
    _description = "Location"


    def local_search(self, cr, uid, args, offset=0, limit=None, order=None,
            context=None, count=False):

        print ' yyyyyyyyyyyyyyy: local search args ' + str(args) + ' ------- ' + str(context)
        if context is None:
            context = {}

        # passed in via domain query within xml file
        result = [];

        if 'default_audio_id' in context:
            print ' yyyyyyyyyyyyyyy: args ' + str(args)
            default_audio_id = context['default_audio_id']

            pf_obj = self.pool.get("cc.facility.audio")
            audios = pf_obj.browse(cr, uid, [default_audio_id], {})
            for audio in audios:
                facility_id = audio.facility_id.id
                print ' yyyyyyyyyyyyy: append locations ' + str(facility_id)
                result.append(facility_id)

        if 'default_encoder_id' in context:
            print ' yyyyyyyyyyyyyyy: args ' + str(args)
            default_encoder_id = context['default_encoder_id']

            pf_obj = self.pool.get("cc.facility.encoder")
            encoders = pf_obj.browse(cr, uid, [default_encoder_id], {})
            for encoder in encoders:
                facility_id = encoder.facility_id.id
                print ' yyyyyyyyyyyyy: append locations ' + str(facility_id)
                result.append(facility_id)

        if 'partner_id' in context:
            partner_id = context['partner_id']

            pf_obj = self.pool.get("cc.res.partner.facility")
            partner_facility_ids = pf_obj.search(cr, uid, [('partner_id', '=', partner_id)], {})

            partner_facilities = pf_obj.browse(cr, uid, partner_facility_ids, {})

            for par_fac in partner_facilities:
                result.append(par_fac.facility_id.id)

        return result



    def name_search(self, cr, user, name='', args=None, operator='ilike',
                             context=None, limit=100):

        print ' yyyyyyyyyyyyyyy: name  search '

        if not args:
            args = []

        ids = []

        if ('partner_id' in context):
            ids.extend(self.local_search(cr, user, args, limit=limit, context=context))

        else:
            ids.extend(self.search(cr, user, args, limit=limit, context=context))

        results = self.name_get(cr, user, ids, context)
        return sorted(results, key=lambda (id, name): ids.index(id))

    _columns = {
        'name': fields.char('Name', required=True, select=True),
        'active': fields.boolean('Active'),
        'legacy_id': fields.char('Legacy Id'),

        'audio_ids': fields.one2many('cc.facility.audio', 'facility_id', 'Audio Sources'),
        'encoder_ids': fields.one2many('cc.facility.encoder', 'facility_id', 'Encoders'),

        'partner_ids': fields.one2many('cc.res.partner.facility', 'facility_id', 'Customers'),

        'contact_ids': fields.one2many('cc.facility.contact', 'facility_id', 'Contacts'),
    }
    _defaults = {
        'active': lambda *a: 1,
    }

class cc_facility(models.Model):
    _inherit = 'cc.facility'



class cc_preset(osv.osv):

    """ Note """
    _name = 'cc.preset'
    _description = "Program Facility"


    def _get_audios(self, cr, uid, ids, field_name, arg, context=None):

        result = {}
        if not ids: return result

        pres_obj = self.pool.get("cc.preset")
        presets = pres_obj.browse(cr, uid, ids, context=context)
        for pres in presets:
            result.setdefault(pres.id, [])

            pf_obj = self.pool.get("cc.res.partner.facility")
            partner_facility_ids = pf_obj.search(cr, uid, [('partner_id', '=', pres.partner_id.id)], context=context)
            partner_facilities = pf_obj.browse(cr, uid, partner_facility_ids, context=context)
            for par_fac in partner_facilities:
                for aud in par_fac.facility_id.audio_ids:
                    result[pres.id].append(aud.id)

        return result

    def _get_audio_addresses(self, cr, uid, ids, field_name, arg, context=None):

        result = {}
        if not ids: return result

        pres_obj = self.pool.get("cc.preset")
        presets = pres_obj.browse(cr, uid, ids, context=context)
        for pres in presets:
            result.setdefault(pres.id, [])

            for add in pres.audio_id.audio_address_ids:
                result[pres.id].append(add.id)

        return result

    def _get_secondary_audio_addresses(self, cr, uid, ids, field_name, arg, context=None):

        result = {}
        if not ids: return result

        pres_obj = self.pool.get("cc.preset")
        presets = pres_obj.browse(cr, uid, ids, context=context)
        for pres in presets:
            result.setdefault(pres.id, [])

            for add in pres.secondary_audio_id.audio_address_ids:
                result[pres.id].append(add.id)

        return result

    def _get_encoder_addresses(self, cr, uid, ids, field_name, arg, context=None):

        result = {}
        if not ids: return result

        pres_obj = self.pool.get("cc.preset")
        presets = pres_obj.browse(cr, uid, ids, context=context)
        for pres in presets:
            result.setdefault(pres.id, [])

            for add in pres.encoder_id.encoder_address_ids:
                result[pres.id].append(add.id)

        return result

    def _get_secondary_encoder_addresses(self, cr, uid, ids, field_name, arg, context=None):

        result = {}
        if not ids: return result

        pres_obj = self.pool.get("cc.preset")
        presets = pres_obj.browse(cr, uid, ids, context=context)
        for pres in presets:
            result.setdefault(pres.id, [])

            for add in pres.secondary_encoder_id.encoder_address_ids:
                result[pres.id].append(add.id)

        return result

    _columns = {
        'name': fields.char('Name', required=True, select=True),
        'concurrent_event_count': fields.integer('Concurrent Event Count'),
        'active': fields.boolean('Active'),
        'legacy_id': fields.char('Legacy Id'),

        'vchip_enabled': fields.boolean('VChip Enabled'),
        'amplify_enabled': fields.boolean('Amplify Enabled'),

        'copy_forward': fields.boolean('Copy Forward'),
        'temp_copy_forward': fields.boolean('Temp Copy Forward'),

        'audio_id': fields.many2one('cc.facility.audio', 'Primary Audio', ondelete='cascade' ),
        'audio_sip': fields.related('audio_id', 'sip', type='char', string='SIP', readonly=True),
        'audio_ip_audio': fields.related('audio_id', 'ip_audio', type='char', string='IP Audio', readonly=True),
        'audio_address_ids': fields.function(_get_audio_addresses, type='one2many', obj='cc.facility.audio.address', string="Audio Addresses"),

        'secondary_audio_id': fields.many2one('cc.facility.audio', 'Secondary Audio', ondelete='cascade'),
        'secondary_audio_sip': fields.related('secondary_audio_id', 'sip', type='char', string='SIP', readonly=True),
        'secondary_audio_ip_audio': fields.related('secondary_audio_id', 'ip_audio', type='char', string='IP Audio', readonly=True),
        'secondary_audio_address_ids': fields.function(_get_secondary_audio_addresses, type='one2many', obj='cc.facility.audio.address', string="Audio Addresses"),

        'encoder_id': fields.many2one('cc.facility.encoder', 'Primary Encoder', ondelete='cascade'),
        'encoder_dial_up': fields.related('encoder_id', 'dial_up', type='char', string='Dial-up', readonly=True),
        'encoder_vpn': fields.related('encoder_id', 'vpn', type='char', string='VPN', readonly=True),
        'encoder_address_ids': fields.function(_get_encoder_addresses, type='one2many', obj='cc.facility.encoder.address', string="Encoder Addresses"),
        'encoder_driver_name': fields.related('encoder_id', 'driver_name', type='char', string='Encoder Driver Name', readonly=True),

        'secondary_encoder_id': fields.many2one('cc.facility.encoder', 'Secondary Encoder', ondelete='cascade'),
        'secondary_encoder_dial_up': fields.related('secondary_encoder_id', 'dial_up', type='char', string='Dial-up', readonly=True),
        'secondary_encoder_vpn': fields.related('secondary_encoder_id', 'vpn', type='char', string='VPN', readonly=True),
        'secondary_encoder_address_ids': fields.function(_get_secondary_encoder_addresses, type='one2many', obj='cc.facility.encoder.address', string="Encoder Addresses"),
        'secondary_encoder_driver_name': fields.related('secondary_encoder_id', 'driver_name', type='char', string='Encoder Driver Name', readonly=True),

        'note': fields.text('Notes'),

        'partner_id': fields.many2one('res.partner', 'Customer', ondelete='cascade', required=True),

        'audio_ids': fields.function(_get_audios, type='one2many', obj='cc.facility.audio', string="Audio Sources"),

    }
    _defaults = {
        'active': lambda *a: 1,
    }


class cc_facility_audio(osv.osv):
    """ Audio """
    _name = 'cc.facility.audio'
    _description = "Audio"

    def read(self, cr, user, ids, fields=None, context=None, load='_classic_read'):
        return super(cc_facility_audio, self).read(cr, user, ids, fields, context, load)

    def browse(self, cr, uid, arg=None, context=None):
        return super(cc_facility_audio, self).browse(cr, uid, arg, context)

    def _audio_address_text(self, cr, uid, ids, field_name, arg, context=None):
        res = dict.fromkeys(ids, '')
        for audio in self.browse(cr, uid, ids, context=context):
            for address in audio.audio_address_ids:
                res[audio.id] += address.name + '<br />'
        return res

    def search(self, cr, uid, args, offset=0, limit=None, order=None,
            context=None, count=False):

        if context is None:
            context = {}

        if ('params' in context):
            params = context['params']
            if ('id' in params):
                partner_id = params['id']

                result = [];
                pf_obj = self.pool.get("cc.res.partner.facility")
                partner_facility_ids = pf_obj.search(cr, uid, [('partner_id', '=', partner_id)], {})

                partner_facilities = pf_obj.browse(cr, uid, partner_facility_ids, {})

                for par_fac in partner_facilities:

                    for aud in par_fac.facility_id.audio_ids:
                        result.append(aud.id)

                return result

        if ('default_partner_id' in context):
            partner_id = context['default_partner_id']

            result = [];
            pf_obj = self.pool.get("cc.res.partner.facility")
            partner_facility_ids = pf_obj.search(cr, uid, [('partner_id', '=', partner_id)], context=context)

            partner_facilities = pf_obj.browse(cr, uid, partner_facility_ids, {})

            for par_fac in partner_facilities:

                for aud in par_fac.facility_id.audio_ids:
                    result.append(aud.id)

            return result

        # passed in via context within xml file
        if ('default_preset_id' in context):

            default_preset_id = context['default_preset_id']

            result = [];
            pf_obj = self.pool.get("cc.preset")
            presets = pf_obj.browse(cr, uid, [default_preset_id], {})
            for preset in presets:
                partner_id = preset.partner_id.id

                result = [];
                pf_obj = self.pool.get("cc.res.partner.facility")
                partner_facility_ids = pf_obj.search(cr, uid, [('partner_id', '=', partner_id)], {})

                partner_facilities = pf_obj.browse(cr, uid, partner_facility_ids, {})
                for par_fac in partner_facilities:
                    for aud in par_fac.facility_id.audio_ids:
                        result.append(aud.id)

            return result

        # passed in via domain query within xml file
        if ('default_preset_id' in args):

            default_preset_id = args['default_preset_id']

            result = [];
            pf_obj = self.pool.get("cc.preset")
            presets = pf_obj.browse(cr, uid, [default_preset_id], {})
            for preset in presets:
                partner_id = preset.partner_id.id

                result = [];
                pf_obj = self.pool.get("cc.res.partner.facility")
                partner_facility_ids = pf_obj.search(cr, uid, [('partner_id', '=', partner_id)], {})

                partner_facilities = pf_obj.browse(cr, uid, partner_facility_ids, {})
                for par_fac in partner_facilities:
                    for aud in par_fac.facility_id.audio_ids:
                        result.append(aud.id)


            return result

        return super(cc_facility_audio, self).search(cr, uid, args, offset, limit, order, context, count)


    def name_search(self, cr, user, name='', args=None, operator='ilike',
                             context=None, limit=100):
        if not args:
            args = []

        ids = []
        ids.extend(self.search(cr, user, args, limit=limit, context=context))

        results = self.name_get(cr, user, ids, context)
        return sorted(results, key=lambda (id, name): ids.index(id))



    _columns = {
        'name': fields.char('Name', required=True, select=True),
        'facility_id': fields.many2one('cc.facility', 'Location', ondelete='cascade', required=True),
        'partner_id': fields.many2one('res.partner', 'Customer'),
        'active': fields.boolean('Active'),
        'sip': fields.boolean('SIP'),
        'ip_audio': fields.boolean('IP Audio'),
        'note': fields.text('Notes'),
        'audio_address_ids': fields.one2many('cc.facility.audio.address', 'audio_id', 'Audio Addresses'),

        'legacy_id': fields.char('Legacy Id'),
    }
    _defaults = {
        'active': lambda *a: 1,
    }

class cc_facility_encoder(osv.osv):
    """ Encoder """
    _name = 'cc.facility.encoder'
    _description = "Encoder"



    def read(self, cr, user, ids, fields=None, context=None, load='_classic_read'):
        return super(cc_facility_encoder, self).read(cr, user, ids, fields, context, load)

    def browse(self, cr, uid, arg=None, context=None):
        return super(cc_facility_encoder, self).browse(cr, uid, arg, context)

    def search(self, cr, uid, args, offset=0, limit=None, order=None,
            context=None, count=False):

        if context is None:
            context = {}

        if ('params' in context):
            params = context['params']
            if ('id' in params):
                partner_id = params['id']

                result = [];
                pf_obj = self.pool.get("cc.res.partner.facility")
                partner_facility_ids = pf_obj.search(cr, uid, [('partner_id', '=', partner_id)], {})

                partner_facilities = pf_obj.browse(cr, uid, partner_facility_ids, {})

                for par_fac in partner_facilities:

                    for enc in par_fac.facility_id.encoder_ids:
                        result.append(enc.id)

                return result


        if ('default_partner_id' in context):
            partner_id = context['default_partner_id']

            result = [];
            pf_obj = self.pool.get("cc.res.partner.facility")
            partner_facility_ids = pf_obj.search(cr, uid, [('partner_id', '=', partner_id)], {})

            partner_facilities = pf_obj.browse(cr, uid, partner_facility_ids, {})

            for par_fac in partner_facilities:

                for enc in par_fac.facility_id.encoder_ids:
                    result.append(enc.id)

            return result

        # passed in via context within xml file
        if ('default_preset_id' in context):

            default_preset_id = context['default_preset_id']

            result = [];
            pf_obj = self.pool.get("cc.preset")
            presets = pf_obj.browse(cr, uid, [default_preset_id], {})
            for preset in presets:
                partner_id = preset.partner_id.id

                result = [];
                pf_obj = self.pool.get("cc.res.partner.facility")
                partner_facility_ids = pf_obj.search(cr, uid, [('partner_id', '=', partner_id)], {})

                partner_facilities = pf_obj.browse(cr, uid, partner_facility_ids, {})
                for par_fac in partner_facilities:
                    for enc in par_fac.facility_id.encoder_ids:
                        result.append(enc.id)

            return result

        # passed in via domain query within xml file
        if ('default_preset_id' in args):

            default_preset_id = args['default_preset_id']

            result = [];
            pf_obj = self.pool.get("cc.preset")
            presets = pf_obj.browse(cr, uid, [default_preset_id], {})
            for preset in presets:
                partner_id = preset.partner_id.id

                result = [];
                pf_obj = self.pool.get("cc.res.partner.facility")
                partner_facility_ids = pf_obj.search(cr, uid, [('partner_id', '=', partner_id)], {})

                partner_facilities = pf_obj.browse(cr, uid, partner_facility_ids, {})
                for par_fac in partner_facilities:
                    for enc in par_fac.facility_id.encoder_ids:
                        result.append(enc.id)


            return result

        return super(cc_facility_encoder, self).search(cr, uid, args, offset, limit, order, context, count)


    def name_search(self, cr, user, name='', args=None, operator='ilike',
                             context=None, limit=100):
        if not args:
            args = []

        ids = []
        ids.extend(self.search(cr, user, args, limit=limit, context=context))

        results = self.name_get(cr, user, ids, context)
        return sorted(results, key=lambda (id, name): ids.index(id))



    _columns = {
        'name': fields.char('Name', required=True, select=True),
        'partner_id': fields.many2one('res.partner', 'Customer'),
        'facility_id': fields.many2one('cc.facility', 'Location', ondelete='cascade', required=True),
        'encoder_type_id': fields.many2one('cc.facility.encoder.type', 'Encoder Type'),
        'protocol_id': fields.many2one('cc.facility.encoder.protocol', 'Encoder Protocol'),
        'active': fields.boolean('Active'),
        'dial_up': fields.boolean('Dial up'),
        'vpn': fields.boolean('VPN'),
        'note': fields.text('Notes'),
        'ccc_version': fields.char('CCC Version'),
        'driver_name': fields.char('Driver Name'),
        'ack_alert_max_seconds': fields.integer('ACK Alert Max Seconds'),
        'encoder_address_ids': fields.one2many('cc.facility.encoder.address', 'encoder_id', 'Encoder Addresses'),
        'legacy_id': fields.char('Legacy Id'),
    }
    _defaults = {
        'active': lambda *a: 1,
    }

class cc_facility_encoder_type(osv.osv):
    """ Encoder """
    _name = 'cc.facility.encoder.type'
    _description = "Encoder Type"

    _columns = {
        'name': fields.char('Name', required=True, select=True),
        'note': fields.text('Notes'),
        'manufacturer': fields.char('Manufacturer'),
        'driver_name': fields.char('Driver Name'),
        'model': fields.char('Model'),
        'legacy_id': fields.char('Legacy Id'),
    }
    _defaults = {
    }

class cc_facility_encoder_protocol(osv.osv):
    """ Encoder """
    _name = 'cc.facility.encoder.protocol'
    _description = "Encoder Protocol"

    _columns = {
        'name': fields.char('Name', required=True, select=True),
        'concurrent_event_count': fields.integer('Concurrent Event Count'),
        'legacy_id': fields.char('Legacy Id'),
    }
    _defaults = {
    }

class cc_facility_encoder_address(osv.osv):
    """ Encoder Address """
    _name = 'cc.facility.encoder.address'
    _description = "Encoder Address"

    _columns = {
        'name': fields.char('Name', required=True, select=True),
        'encoder_id': fields.many2one('cc.facility.encoder', 'Encoder', ondelete='cascade', required=True),
        'encoder_address_type_id': fields.many2one('cc.facility.encoder.address.type', 'Encoder'),
        'ccc_enabled': fields.boolean('CCC Enabled'),
        'allow_auto_monitor': fields.boolean('Allow Auto Monitor'),
        'init_string': fields.char('Init String'),
        'active': fields.boolean('Active'),
        'vpn_required': fields.boolean('VPN Required'),
        'system_guid': fields.char('System GUID'),
        'legacy_id': fields.char('Legacy Id'),
    }
    _defaults = {
    }

class cc_facility_encoder_address_type(osv.osv):
    """ Encoder Address Type """
    _name = 'cc.facility.encoder.address.type'
    _description = "Encoder Address Type"

    _columns = {
        'name': fields.char('Name', required=True, select=True),
    }
    _defaults = {
    }



class cc_facility_audio_address(osv.osv):
    """ Audio Address """
    _name = 'cc.facility.audio.address'
    _description = "Audio Source Address"

    _columns = {
        'name': fields.char('Name', required=True, select=True),
        'audio_id': fields.many2one('cc.facility.audio', 'Audio', ondelete='cascade', required=True),
        'audio_address_type_id': fields.many2one('cc.facility.audio.address.type', 'Address Type'),
        'active': fields.boolean('Active'),
        'legacy_id': fields.char('Legacy Id'),
    }
    _defaults = {
    }

class cc_facility_audio_address_type(osv.osv):
    """ Audio Address Type """
    _name = 'cc.facility.audio.address.type'
    _description = "Audio Address Type"

    _columns = {
        'name': fields.char('Name', required=True, select=True),
    }
    _defaults = {
    }


class cc_res_partner_facility(osv.osv):
    '''Customer Facilities'''
    _name = "cc.res.partner.facility"
    _description = __doc__
    _order = 'sequence'


    _columns = {
        'name': fields.char('Customer Facility'),
        'facility_id': fields.many2one('cc.facility', 'Location', ondelete='cascade', required=True),
        'partner_id': fields.many2one('res.partner', 'Customer', ondelete='cascade', select=True),
        'sequence': fields.integer('Sequence'),
    }

    _defaults = {
        'name': '/'
    }


    def onchange_facility_id(self, cr, uid, ids, facility_id, context=None):
        context = context or {}
        result = {}

        partner_id = False;
        if 'default_partner_id' in context:
            partner_id = context["default_partner_id"]

        partner_obj = self.pool.get('res.partner')

        if facility_id:
            facility = self.pool.get('cc.facility').browse(cr, uid, facility_id, context=context)
            result['facility_name'] = facility.name
            #result['facility_code'] = facility.code

        if partner_id and facility_id:
            part_obj = partner_obj.browse(cr, uid, partner_id, context=context)
            facility_obj = self.pool.get('cc.facility').browse(cr, uid, facility_id, context=context)

            partnerName = part_obj.name or ''
            facilityName = facility_obj.name or ''
            result['name'] =  partnerName + " - " + facilityName

        return {'value': result}

    def onchange_company_id(self, cr, uid, ids, company_id, context=None):
        result = {}
        if company_id:
            c = self.pool.get('res.company').browse(cr, uid, company_id, context=context)
            if c.partner_id:
                r = self.onchange_partner_id(cr, uid, ids, c.partner_id.id, context=context)
                r['value']['partner_id'] = c.partner_id.id
                result = r
        return result

    def onchange_partner_id(self, cr, uid, ids, partner_id, context=None):
        result = {}
        if partner_id:
            part = self.pool.get('res.partner').browse(cr, uid, partner_id, context=context)
        return {'value': result}


class cc_facility_contact(osv.osv):

    _name = "cc.facility.contact"
    _description = __doc__
    _order = 'sequence'


    _columns = {
        'facility_id': fields.many2one('cc.facility', 'Location', ondelete='cascade', required=True),
        'contact_id': fields.many2one('res.partner', 'Contact', ondelete='cascade', select=True, domain=[('is_company','=', False)]),
        'sequence': fields.integer('Sequence'),
        'phone': fields.related('contact_id', 'phone', type='char', string='phone', readonly=True),
        'email': fields.related('contact_id', 'email', type='char', string='email', readonly=True),
    }

    _defaults = {
    }

class res_partner(osv.osv):
    _inherit = 'res.partner'

    def _get_facs(self, cr, uid, ids, field_name, arg, context=None):

        result = {}
        if not ids: return result


        for partner_id in ids:
            result.setdefault(partner_id, [])

            pf_obj = self.pool.get("cc.res.partner.facility")
            partner_facility_ids = pf_obj.search(cr, uid, [('partner_id', '=', partner_id)], context=context)
            partner_facilities = pf_obj.browse(cr, uid, partner_facility_ids, context=context)
            for par_fac in partner_facilities:
                result[partner_id].append(par_fac.facility_id.id)

        return result


    def _get_audiosources(self, cr, uid, ids, field_name, arg, context=None):

        result = {}
        if not ids: return result


        for partner_id in ids:
            result.setdefault(partner_id, [])

            pf_obj = self.pool.get("cc.res.partner.facility")
            partner_facility_ids = pf_obj.search(cr, uid, [('partner_id', '=', partner_id)], context=context)
            partner_facilities = pf_obj.browse(cr, uid, partner_facility_ids, context=context)
            for par_fac in partner_facilities:
                for aud in par_fac.facility_id.audio_ids:
                    result[partner_id].append(aud.id)

        return result

    def _get_encoders(self, cr, uid, ids, field_name, arg, context=None):

        result = {}
        if not ids: return result


        for partner_id in ids:
            result.setdefault(partner_id, [])

            pf_obj = self.pool.get("cc.res.partner.facility")
            partner_facility_ids = pf_obj.search(cr, uid, [('partner_id', '=', partner_id)], context=context)
            partner_facilities = pf_obj.browse(cr, uid, partner_facility_ids, context=context)
            for par_fac in partner_facilities:
                for enc in par_fac.facility_id.encoder_ids:
                    result[partner_id].append(enc.id)

        return result

    def _get_fac_names(self, cr, uid, ids, field_name, arg, context=None):

        res = {}

        for top_fac_id in ids:
            top_par_fac_obj = self.pool.get("cc.res.partner.facility")
            top_par_fac_ids = top_par_fac_obj.search(cr, uid, [('facility_id', '=', top_fac_id)], context=context)

            top_par_facs = top_par_fac_obj.browse(cr, uid, top_par_fac_ids, context=context)

            for top_par_fac in top_par_facs:

                par_id = top_par_fac.partner_id.id

                res[par_id] = ""

                par_fac = self.pool.get("cc.res.partner.facility")

                par_fac_ids = par_fac.search(cr, uid, [('partner_id', '=', par_id)], context=context)
                par_facs = top_par_fac_obj.browse(cr, uid, par_fac_ids, context=context)
                for par_fac in par_facs:
                    res[par_id] = res[par_id] + par_fac.facility_id.name

        return res


    def _get_encoder_names(self, cr, uid, ids, field_name, arg, context=None):

        # facility ids are passed in for encoders that have changed,  need to reset associated customer encoder names
        res = {}


        for top_fac_id in ids:
            top_par_fac_obj = self.pool.get("cc.res.partner.facility")
            top_par_fac_ids = top_par_fac_obj.search(cr, uid, [('facility_id', '=', top_fac_id)], context=context)

            top_par_facs = top_par_fac_obj.browse(cr, uid, top_par_fac_ids, context=context)

            for top_par_fac in top_par_facs:

                par_id = top_par_fac.partner_id.id

                res[par_id] = ""

                par_fac = self.pool.get("cc.res.partner.facility")

                par_fac_ids = par_fac.search(cr, uid, [('partner_id', '=', par_id)], context=context)
                par_facs = top_par_fac_obj.browse(cr, uid, par_fac_ids, context=context)
                for par_fac in par_facs:
                    fac_enc = self.pool.get("cc.facility.encoder")
                    enc_ids = fac_enc.search(cr, uid, [('facility_id', 'in', [par_fac.facility_id.id])], context=context)

                    encs = fac_enc.read(cr, uid, enc_ids, [('name')], context=context)

                    for enc in encs:
                        res[par_id] = res[par_id] + enc['name']


        return res

    def _get_ccc_flag(self, cr, uid, ids, field_name, arg, context=None):

        # facility ids are passed in for encoders that have changed,  need to reset associated customer encoder names
        res = {}


        for top_fac_id in ids:
            top_par_fac_obj = self.pool.get("cc.res.partner.facility")
            top_par_fac_ids = top_par_fac_obj.search(cr, uid, [('facility_id', '=', top_fac_id)], context=context)

            top_par_facs = top_par_fac_obj.browse(cr, uid, top_par_fac_ids, context=context)

            for top_par_fac in top_par_facs:

                par_id = top_par_fac.partner_id.id

                res[par_id] = ""

                par_fac = self.pool.get("cc.res.partner.facility")

                par_fac_ids = par_fac.search(cr, uid, [('partner_id', '=', par_id)], context=context)
                par_facs = top_par_fac_obj.browse(cr, uid, par_fac_ids, context=context)
                for par_fac in par_facs:
                    fac_enc = self.pool.get("cc.facility.encoder")
                    enc_ids = fac_enc.search(cr, uid, [('facility_id', 'in', [par_fac.facility_id.id])], context=context)

                    encs = fac_enc.browse(cr, uid, enc_ids, context=context)
                    print '*********** _get_encoders names  pars ' + str(fac_enc)
                    for enc in encs:
                        ccc_enc = self.pool.get("cc.facility.encoder.address")
                        ccc_ids = ccc_enc.search(cr,uid,[('ccc_enabled', 'in', [enc.encoder_address_ids.id])], context=context)\

                        print '*********** _get_ccc names  pars ' + str(enc.encoder_address_ids.id)
                        cccs = ccc_enc.browse(cr, uid, ccc_ids, context=context)
                        for ccc in cccs:
                            print '*********** _get_ccc names  pars ' + str(ccc['ccc_enabled'])
                            res[par_id] = res[par_id] + str(ccc['ccc_enabled'])


        return res


    def _get_encoder_address(self, cr, uid, ids, field_name, arg, context=None):

        # facility ids are passed in for encoders that have changed,  need to reset associated customer encoder names
        res = {}


        for top_fac_id in ids:
            top_par_fac_obj = self.pool.get("cc.res.partner.facility")
            top_par_fac_ids = top_par_fac_obj.search(cr, uid, [('facility_id', '=', top_fac_id)], context=context)

            top_par_facs = top_par_fac_obj.browse(cr, uid, top_par_fac_ids, context=context)

            for top_par_fac in top_par_facs:

                par_id = top_par_fac.partner_id.id

                res[par_id] = ""

                par_fac = self.pool.get("cc.res.partner.facility")

                par_fac_ids = par_fac.search(cr, uid, [('partner_id', '=', par_id)], context=context)
                par_facs = top_par_fac_obj.browse(cr, uid, par_fac_ids, context=context)
                for par_fac in par_facs:
                    fac_enc = self.pool.get("cc.facility.encoder")
                    enc_ids = fac_enc.search(cr, uid, [('facility_id', 'in', [par_fac.facility_id.id])], context=context)

                    encs = fac_enc.browse(cr, uid, enc_ids, context=context)
                    print '*********** _get_encoders names  pars ' + str(fac_enc)
                    for enc in encs:
                        ccc_enc = self.pool.get("cc.facility.encoder.address")
                        ccc_ids = ccc_enc.search(cr,uid,[('encoder_id', 'in', [enc.encoder_address_ids.id])], context=context)

                        print '*********** _get_address  pars ' + str(enc.encoder_address_ids.id)
                        cccs = ccc_enc.browse(cr, uid, ccc_ids, context=context)
                        for ccc in cccs:
                            print '*********** _get_address  pars ' + ccc['name']
                            res[par_id] = res[par_id] + ccc['name']


        return res

    def _get_audio_names(self, cr, uid, ids, field_name, arg, context=None):

        res = {}


        for top_fac_id in ids:
            top_par_fac_obj = self.pool.get("cc.res.partner.facility")
            top_par_fac_ids = top_par_fac_obj.search(cr, uid, [('facility_id', '=', top_fac_id)], context=context)

            top_par_facs = top_par_fac_obj.browse(cr, uid, top_par_fac_ids, context=context)

            for top_par_fac in top_par_facs:

                par_id = top_par_fac.partner_id.id

                res[par_id] = ""

                par_fac = self.pool.get("cc.res.partner.facility")

                par_fac_ids = par_fac.search(cr, uid, [('partner_id', '=', par_id)], context=context)
                par_facs = top_par_fac_obj.browse(cr, uid, par_fac_ids, context=context)
                for par_fac in par_facs:

                    fac_aud = self.pool.get("cc.facility.audio")
                    aud_ids = fac_aud.search(cr, uid, [('facility_id', 'in', [par_fac.facility_id.id])], context=context)

                    auds = fac_aud.read(cr, uid, aud_ids, [('name')], context=context)

                    for aud in auds:
                        res[par_id] = res[par_id] + aud['name']


        return res

    def _get_vpn_flag(self, cr, uid, ids, field_name, arg, context=None):

        # facility ids are passed in for encoders that have changed,  need to reset associated customer encoder names
        res = {}


        for top_fac_id in ids:
            top_par_fac_obj = self.pool.get("cc.res.partner.facility")
            top_par_fac_ids = top_par_fac_obj.search(cr, uid, [('facility_id', '=', top_fac_id)], context=context)

            top_par_facs = top_par_fac_obj.browse(cr, uid, top_par_fac_ids, context=context)

            for top_par_fac in top_par_facs:

                par_id = top_par_fac.partner_id.id

                res[par_id] = ""

                par_fac = self.pool.get("cc.res.partner.facility")

                par_fac_ids = par_fac.search(cr, uid, [('partner_id', '=', par_id)], context=context)
                par_facs = top_par_fac_obj.browse(cr, uid, par_fac_ids, context=context)
                for par_fac in par_facs:
                    fac_enc = self.pool.get("cc.facility.encoder")
                    enc_ids = fac_enc.search(cr, uid, [('facility_id', 'in', [par_fac.facility_id.id])], context=context)

                    encs = fac_enc.browse(cr, uid, enc_ids, context=context)
                    print '*********** _get_encoders names  pars ' + str(fac_enc)
                    for enc in encs:
                        vpn_enc = self.pool.get("cc.facility.encoder.address")
                        vpn_ids = vpn_enc.search(cr,uid,[('vpn_required', 'in', [enc.encoder_address_ids.id])], context=context)\

                        print '*********** _get_vpn names  pars ' + str(enc.encoder_address_ids.id)
                        vpns = vpn_enc.browse(cr, uid, vpn_ids, context=context)
                        for vpn in vpns:
                            print '*********** _get_vpn names  pars ' + str(vpn['vpn_required'])
                            res[par_id] = res[par_id] + str(vpn['vpn_required'])


        return res

    def _get_encoder_types(self, cr, uid, ids, field_name, arg, context=None):

        # facility ids are passed in for encoders that have changed,  need to reset associated customer encoder names
        res = {}

        print '*********** _get_encoders names  ' + str(field_name) + ' -- ' + str(ids) + ' -- ' + str(arg) + ' -- ' + str(context)

        for top_fac_id in ids:
            top_par_fac_obj = self.pool.get("cc.res.partner.facility")
            top_par_fac_ids = top_par_fac_obj.search(cr, uid, [('facility_id', '=', top_fac_id)], context=context)
            print '*********** _get_encoders names  pars ids ' + str(top_par_fac_ids)
            top_par_facs = top_par_fac_obj.browse(cr, uid, top_par_fac_ids, context=context)

            print '*********** _get_encoders names  pars ' + str(top_par_facs)

            for top_par_fac in top_par_facs:


                par_id = top_par_fac.partner_id.id
                print '*********** _get_encoders names  par ' + str(par_id)

                res[par_id] = ""

                par_fac = self.pool.get("cc.res.partner.facility")

                par_fac_ids = par_fac.search(cr, uid, [('partner_id', '=', par_id)], context=context)
                par_facs = top_par_fac_obj.browse(cr, uid, par_fac_ids, context=context)
                for par_fac in par_facs:
                    print '*********** fac_id  ' + str(par_fac.facility_id.id)

                    fac_enc = self.pool.get("cc.facility.encoder")
                    enc_ids = fac_enc.search(cr, uid, [('facility_id', 'in', [par_fac.facility_id.id])], context=context)

                    encs = fac_enc.browse(cr, uid, enc_ids, context=context)

                    for enc in encs:
                        res[par_id] = res[par_id] + str(enc.encoder_type_id.name)

            print '*********** return names  ' + str(res)

        return res

    def _search_encoders(self, cr, uid, model_again, field_name, criterion, context):

        res = []
        enc_domain = [('name', criterion[0][1], criterion[0][2])]

        enc = self.pool.get("cc.facility.encoder")
        enc_fac = self.pool.get("cc.facility.encoder")


        pids = []

        enc_ids = enc.search(cr, uid, enc_domain, context=context)
        enc_fac_ids = enc_fac.search(cr, uid, [('id', 'in', enc_ids)], context=context)
        for fac_id in enc_fac_ids:
            par_fac = self.pool.get("cc.res.partner.facility")
            partner_facility_ids = par_fac.search(cr, uid, [('facility_id', '=', fac_id)], context=context)
            partner_facilities = par_fac.browse(cr, uid, partner_facility_ids, context=context)
            for par_fac in partner_facilities:
                pids.append(par_fac.partner_id.id)


        return [('id', 'in', pids)]
    def _search_audios(self, cr, uid, model_again, field_name, criterion, context):


        res = []
        aud_domain = [('name', criterion[0][1], criterion[0][2])]

        aud = self.pool.get("cc.facility.audio")
        aud_fac = self.pool.get("cc.facility.audio")


        pids = []

        aud_ids = aud.search(cr, uid, aud_domain, context=context)
        aud_fac_ids = aud_fac.search(cr, uid, [('id', 'in', aud_ids)], context=context)
        for fac_id in aud_fac_ids:
            par_fac = self.pool.get("cc.res.partner.facility")
            partner_facility_ids = par_fac.search(cr, uid, [('facility_id', '=', fac_id)], context=context)
            partner_facilities = par_fac.browse(cr, uid, partner_facility_ids, context=context)
            for par_fac in partner_facilities:
                pids.append(par_fac.partner_id.id)

        return [('id', 'in', pids)]

    def _search_facs(self, cr, uid, model_again, field_name, criterion, context):


        res = []
        fac_domain = [('name', criterion[0][1], criterion[0][2])]

        fac = self.pool.get("cc.facility")
        fac_fac = self.pool.get("cc.facility")


        pids = []

        fac_ids = fac.search(cr, uid, fac_domain, context=context)
        fac_fac_ids = fac_fac.search(cr, uid, [('id', 'in', fac_ids)], context=context)
        for fac_id in fac_fac_ids:
            par_fac = self.pool.get("cc.res.partner.facility")
            partner_facility_ids = par_fac.search(cr, uid, [('facility_id', '=', fac_id)], context=context)
            partner_facilities = par_fac.browse(cr, uid, partner_facility_ids, context=context)
            for par_fac in partner_facilities:
                pids.append(par_fac.partner_id.id)

        return [('id', 'in', pids)]

    def _search_encoder_types(self, cr, uid, model_again, field_name, criterion, context):

        print '*********** _search_encoders ' + str(self) + ' -- ' + str(model_again) + ' -- ' + str(field_name) + ' -- ' + str(criterion) + ' -- ' + str(context)

        res = []
        enc_domain = [('name', criterion[0][1], criterion[0][2])]

        enc = self.pool.get("cc.facility.encoder.type")
        enc_fac = self.pool.get("cc.facility.encoder.type")


        pids = []

        enc_ids = enc.search(cr, uid, enc_domain, context=context)
        enc_fac_ids = enc_fac.search(cr, uid, [('id', 'in', enc_ids)], context=context)
        for fac_id in enc_fac_ids:
            par_fac = self.pool.get("cc.res.partner.facility")
            partner_facility_ids = par_fac.search(cr, uid, [('facility_id', '=', fac_id)], context=context)
            partner_facilities = par_fac.browse(cr, uid, partner_facility_ids, context=context)
            for par_fac in partner_facilities:
                pids.append(par_fac.partner_id.id)

        print '************ search: ' + str(pids)
        return [('id', 'in', pids)]


    def _search_encoder_address(self, cr, uid, model_again, field_name, criterion, context):

        print '*********** _search_encoders ' + str(self) + ' -- ' + str(model_again) + ' -- ' + str(field_name) + ' -- ' + str(criterion) + ' -- ' + str(context)

        res = []
        enc_domain = [('name', criterion[0][1], criterion[0][2])]

        enc = self.pool.get("cc.facility.encoder.address")
        enc_fac = self.pool.get("cc.facility.encoder.address")


        pids = []

        enc_ids = enc.search(cr, uid, enc_domain, context=context)
        enc_fac_ids = enc_fac.search(cr, uid, [('encoder_id', 'in', enc_ids)], context=context)
        for fac_id in enc_fac_ids:
            par_fac = self.pool.get("cc.res.partner.facility")
            partner_facility_ids = par_fac.search(cr, uid, [('facility_id', '=', fac_id)], context=context)
            partner_facilities = par_fac.browse(cr, uid, partner_facility_ids, context=context)
            for par_fac in partner_facilities:
                pids.append(par_fac.partner_id.id)

        print '************ search: ' + str(pids)
        return [('id', 'in', pids)]



    _columns = {
        'facility_ids': fields.one2many('cc.res.partner.facility', 'partner_id', 'Locations'),
        'preset_ids': fields.one2many('cc.preset', 'partner_id', 'Program Facilities'),
        'fac_ids': fields.function(_get_facs, type='one2many', obj='cc.facility', string="Fac Locations"),
        'audiosource_ids': fields.function(_get_audiosources, type='one2many', obj='cc.facility.audio', string="Audio Sources"),
        'encoder_ids': fields.function(_get_encoders, type='one2many', obj='cc.facility.encoder', string="Encoders"),

        #'encoder_names': fields.function(_get_encoder_names, fnct_search=_search_encoders, type='text', string="Encoder Names"),
        'encoder_names': fields.function(_get_encoder_names, fnct_search=_search_encoders, type='text', string="Encoder Names", store={
            'cc.facility': (lambda self, cr, uid, ids, c={}: ids, ['encoder_ids'], 20),
        }),
        'audiosource_names': fields.function(_get_audio_names, fnct_search=_search_audios, type='text', string="Audio Source Names", store={
            'cc.facility': (lambda self, cr, uid, ids, c={}: ids, ['audio_ids'], 20),
        }),
        'fac_names': fields.function(_get_fac_names, type='text', fnct_search=_search_facs, string="Facility Names", store={
            'cc.facility': (lambda self, cr, uid, ids, c={}: ids, ['partner_ids'], 20),
        }),
        'encoder_types': fields.function(_get_encoder_types, type='text', fnct_search=_search_encoder_types, string="Encoder Types", store={
            'cc.facility': (lambda self, cr, uid, ids, c={}: ids, ['encoder_ids'], 20),
        }),
        'ccc_enabled_flag': fields.function(_get_ccc_flag, type='text', string="CCC Flag", store={
            'cc.facility': (lambda self, cr, uid, ids, c={}: ids, ['encoder_ids'], 20),
        }),
        'vpn_required_flag': fields.function(_get_vpn_flag, type='text', string="VPN Required Flag", store={
            'cc.facility': (lambda self, cr, uid, ids, c={}: ids, ['encoder_ids'], 20),
        }),
        'encoder_address_phone': fields.function(_get_encoder_address, fnct_search=_search_encoder_address, type='text', string="Encoder Address Phone", store={
            'cc.facility': (lambda self, cr, uid, ids, c={}: ids, ['encoder_ids'], 20),
        }),
    }



