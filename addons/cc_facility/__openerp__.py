# -*- coding: utf-8 -*-

{
    'name': 'Caption Colorado Facility',
    'version': '0.1',
    'category': 'Tools',
    'description': """
Custom ODOO module for managing CC Facilities.
=================================================================

Apenware's ODOO framework underpins Caption Colorado's custom ERP, and focuses on managing the information used for scheduling.  Do not uninstall this module, as core capabiltites of the custom ERP will be lost.

""",
    'author': 'Aspenware',
    'category': 'Caption Colorado',
    'website': 'http://www.aspenware.com',
    'summary': 'Caption Colorado Facilities',
    'depends': [ 'cc_partner'],

    'data': [
        'security/ir.model.access.csv',
        'security/cc_facility_security.xml',
        'cc_facility_view.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
