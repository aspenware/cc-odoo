# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import osv, fields
from openerp.tools.translate import _

class product_type(osv.osv):
    """ Audio """
    _name = 'product.type'
    _description = "Product Type"
    _columns = {
        'name': fields.char('Name', required=True, select=True),
    }
    _defaults = {
    }

class product_type_customer_level(osv.osv):
    """ Audio """
    _name = 'product.type.customer.level'
    _description = "Product Type Customer Level"
    _order = 'sequence'
    _columns = {
        'name': fields.char('Name', required=True, select=True),
        'product_type_id': fields.many2one('product.type', 'Product Type'),
        'active': fields.boolean('Active'),
        'level': fields.integer('Level'),
        'note': fields.text('Notes'),
        'sequence': fields.integer('Sequence'),
    }
    _defaults = {
        'active': lambda *a: 1,
    }



class res_partner_product_type_level(osv.osv):
    """ Audio """
    _name = 'res.partner.product.type.level'
    _description = "Customer Level"
    _order = 'sequence'

    def partner_id_change(self, cr, uid, ids, product_level_id, context=None):
        context = context or {}

        partner_id = context["default_partner_id"]

        data = {}
        partner_obj = self.pool.get('res.partner')
        product_level_obj = self.pool.get('product.type.customer.level')

        if partner_id and product_level_id:
            part_obj = partner_obj.browse(cr, uid, partner_id, context=context)
            prod_level_obj = product_level_obj.browse(cr, uid, product_level_id, context=context)

            partnerName = part_obj.name or ''
            prodLevelName = prod_level_obj.name or ''

            product_type = prod_level_obj.product_type_id

            data['product_level_id'] = product_level_id
            data['product_type_id'] = product_type.ids[0]
            data['name'] =  partnerName + " - " + prodLevelName

        return {'value': data}

    _columns = {
        'name': fields.char('Name'),
        'partner_id': fields.many2one('res.partner', 'Customer'),
        'product_level_id': fields.many2one('product.type.customer.level', 'Level'),
        'product_type_id': fields.many2one('product.type', 'Product Type'),
        'active': fields.boolean('Active'),
        'note': fields.text('Notes'),
        'sequence': fields.integer('Sequence'),
    }
    _defaults = {
        'active': lambda *a: 1,
    }





class sale_order(osv.osv):
    _inherit = ['sale.order']
    _columns = {

    }


class sale_order_line(osv.osv):
    _inherit = ['sale.order.line']

    def product_id_change2(self, cr, uid, ids, pricelist, product, qty=0,
            uom=False, qty_uos=0, uos=False, name='', partner_id=False,
            lang=False, update_tax=True, date_order=False, packaging=False, fiscal_position=False, flag=False, context=None):

        product_obj = self.pool.get('product.template')
        prod_obj = product_obj.browse(cr, uid, product, context=context)

        #{'value': result, 'domain': domain, 'warning': warning}
        rtn_info = self.product_id_change(cr, uid, ids, pricelist, product, qty, uom, qty_uos, uos, name, partner_id, lang, update_tax, date_order, packaging, fiscal_position,flag, context)
        value = rtn_info['value']
        value['product_type_id'] = prod_obj.product_type_id
        value['partner_level_id'] = False

        rtn_info['value'] = value

        return rtn_info

    _columns = {
        'partner_level_id': fields.many2one('res.partner.product.type.level', 'Level'),
        'product_type_id': fields.many2one('product.type', 'Product Type'),
    }

class res_partner(osv.osv):
    _inherit = 'res.partner'
    _columns = {
        'product_level_ids': fields.one2many('res.partner.product.type.level', 'partner_id', 'Levels'),
    }

class product_template(osv.osv):
    _inherit = ['product.template']
    _columns = {
        'product_type_id': fields.many2one('product.type', 'Product Type'),
    }