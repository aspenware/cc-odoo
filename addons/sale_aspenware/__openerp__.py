# -*- coding: utf-8 -*-

{
    'name': 'Aspenware Sales',
    'version': '0.1',
    'category': 'Tools',
    'description': """
This module supports managing customer sales
=================================================================

Use for managing customer sales

""",
    'author': 'Aspenware',
    'website': 'http://www.aspenware.com',
    'summary': 'Aspenware Sales',
    'depends': ['sale'],
    'data': [
        'sale_aspenware_view.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
