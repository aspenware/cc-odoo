# -*- coding: utf-8 -*-

{
    'name': 'Caption Colorado Integrator',
    'version': '0.1',
    'category': 'Tools',
    'description': """
This module installs all modules.
=================================================================

Use for managing customer

""",
    'author': 'Aspenware',
    'category': 'Caption Colorado',
    'website': 'http://www.aspenware.com',
    'summary': 'Caption Colorado Customer',
    'depends': [
       'sale',
       'hr',
       'cc_partner',
       'cc_resource',
       'cc_servicetype',
       'cc_facility',
       'cc_auth'
    ],
    'data': [

    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
