import openerp.http as http
from openerp.http import request


class EmployeesController(http.Controller):

    @http.route('/employees', type="json")
    def employees(self):
        return [{"name": "Mike"}, {"name": "Joe"}]