# -*- coding: utf-8 -*-
##############################################################################
#
#    extranet module for OpenERP, External access for OpenERP
#    Copyright (C) 2011 SYLEAM (<http://www.syleam.fr/>)
#              Christophe CHAUVET <christophe.chauvet@syleam.fr>
#
#    This file is a part of extranet
#
#    extranet is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    extranet is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import osv
from openerp.osv import fields

import random
import logging


_logger = logging.getLogger('extranet.wizard')


def gen_password():
    """
    Generate alternate password
    """
    authorized_char = [chr(x) for x in range(48, 58) + range(97, 123) + range(65, 91)]
    for i in ['l','I','O','o','0','1']:
        authorized_char.remove(i)

    pass_chars = authorized_char[:]
    random.shuffle(pass_chars)
    return ''.join(pass_chars[0:10])


class ExternalCreateUser(osv.osv_memory):
    _name = 'extranet.create.user'
    _description = 'Create user for the extranet'

    _columns = {
        'portal_id': fields.many2one('extranet.portal', 'Portal', help='Portal to affect on this user'),
        'address_id': fields.many2one('res.partner', 'Contact', help='Select user to send the new account'),
        'send_mail': fields.boolean('Send email', help='Send generate informations on user ?'),
        'results': fields.text('Result'),
    }

    _defaults = {
         'send_mail': True,
    }

    def step2(self, cr, uid, ids, context=None):
        wizard_data = self.browse(cr, uid, ids and ids[0], context=context)
        _logger.debug('Create user "%s"' % wizard_data.address_id.name)
        if not wizard_data.address_id.email:
            raise osv.except_osv(_('Error'), _('User have no email defined!'))

        user_obj = self.pool.get('res.users')
        user_ids = user_obj.search(cr, uid, [('login', '=', wizard_data.address_id.email)], context=context)
        password = gen_password()
        if user_ids:
            _logger.info('User %s exists, resend the password' % wizard_data.address_id.email)
            args = {
                'password': password,
            }
            user_obj.write(cr, uid, user_ids, args, context=context)
        else:
            _logger.info("User %s doesn't exists, resend the password" % wizard_data.address_id.email)
            args = {
                'name': wizard_data.address_id.name or _('Unknown'),
                'login': wizard_data.address_id.email,
                'password': password,
                'address_id': wizard_data.address_id.id,
                'action_id': wizard_data.portal_id.home_action_id and wizard_data.portal_id.home_action_id.id or wizard_data.portal_id.menu_action_id.id,
                'menu_id': wizard_data.portal_id.menu_action_id.id,
                'groups_id': [(4, wizard_data.portal_id.group_id.id)],
                'company_id': wizard_data.portal_id.company_id.id,
                'share': True,
                'context_lang': wizard_data.address_id.partner_id.lang or 'en_US',
            }
            user_obj.create(cr,uid, args, context=context)

        compose = {
            'username': wizard_data.address_id.name,
            'email': wizard_data.address_id.email,
            'password': password,
            'login': wizard_data.address_id.email,
            'portal': wizard_data.portal_id.name,
            'company': wizard_data.portal_id.company_id.name,
        }
        subject = tools.ustr(wizard_data.portal_id.mail_subject % compose)
        if user_ids:
            subject = _('Fwd: ') + subject
        body = tools.ustr(wizard_data.portal_id.mail_body % compose)
        _logger.debug('subject %s' % subject)
        _logger.debug('body %s' % body)

        if wizard_data.send_mail:
            email_from = tools.config['email_from']
            if not email_from:
                raise osv.except_osv(_('Error'), _('Not mail found for the expeditor!'))

            email_bcc = wizard_data.portal_id.mail_bcci and wizard_data.portal_id.mail_bcci.split(',') or None
            reply_to = wizard_data.portal_id.mail_from or False
            unique_id = 'address%d' % wizard_data.address_id.id

            if not tools.email_send(email_from, [wizard_data.address_id.email] , subject, body,
                                    email_bcc=email_bcc, reply_to=reply_to, openobject_id=unique_id):
                _logger.error('cannot send email to %s !' % wizard_data.address_id.email)
                raise osv.except_osv(_('Error'), _('The email connot be send!'))

        self.write(cr, uid, ids, {'results': body}, context=context)

        dummy, step2_form_view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'extranet', 'view_ext_create_user_step2')
        return {
            'name': _('Results'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'extranet.create.user',
            'view_id': False,
            'res_id': ids[0],
            'views': [(step2_form_view_id, 'form'), (False, 'tree'), (False, 'calendar'), (False, 'graph')],
            'type': 'ir.actions.act_window',
            'target': 'new'
        }

ExternalCreateUser()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
