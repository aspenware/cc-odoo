# -*- coding: utf-8 -*-
##############################################################################
#
#    extranet module for OpenERP, External access for OpenERP
#    Copyright (C) 2011 SYLEAM (<http://www.syleam.fr/>)
#              Christophe CHAUVET <christophe.chauvet@syleam.fr>
#
#    This file is a part of extranet
#
#    extranet is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    extranet is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'Extranet',
    'version': '0.1.0',
    'category': 'Custom',
    'description': """External access for OpenERP""",
    'author': 'SYLEAM',
    'website': 'http://www.syleam.fr/',
    'images': ['images/accueil.png'],
    'depends': ['share'],
    'init_xml': [],
    'update_xml': [
        'security/groups.xml',
        'security/rules.xml',
        'security/ir.model.access.csv',
        'view/menu.xml',
        'view/extranet.xml',
        'view/portal.xml',
        'wizard/create_user.xml',
        #'wizard/wizard.xml',
        #'report/report.xml',
    ],
    'demo_xml': [
        'demo/groups.xml',
        'demo/menus.xml',
    ],
    'installable': True,
    'active': False,
    'license': 'AGPL-3',
}


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
