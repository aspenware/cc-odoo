# -*- coding: utf-8 -*-
##############################################################################
#
#    extranet module for OpenERP, External access for OpenERP
#    Copyright (C) 2011 SYLEAM (<http://www.syleam.fr/>)
#              Christophe CHAUVET <christophe.chauvet@syleam.fr>
#
#    This file is a part of extranet
#
#    extranet is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    extranet is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import osv
from openerp.osv import fields



class ExtranetPortal(osv.osv):
    _name = 'extranet.portal'
    _description = 'Extranet portal configuration'

    _columns = {
        'name': fields.char('Portal Name',size=64, required=True),
        'group_id': fields.many2one('res.groups', 'Associated Group', required=True),
        'menu_id': fields.many2one('ir.ui.menu','Main Menu', required=True),
        'menu_action_id': fields.many2one('ir.actions.act_window', 'User Menu Action', readonly=True,
            help='''Default main menu for the users of the portal. This field is auto-completed at creation. '''),
        'home_action_id': fields.many2one('ir.actions.act_window', 'User Home Action', help='Complete this field to provide a Home menu different from the Main menu.'),
        'company_id': fields.many2one('res.company', 'Company', required=True),
        'mail_from': fields.char('From', size=256, help='Mail address for expeditor, leave blank to use server configuration'),
        'mail_bcci': fields.char('Bcci', size=64, help='Mail list to send a invisible copy of this email'),
        'mail_subject': fields.char('Mail subject', size=256, help='Enter the subject of the mail was send when create user on this portal'),
        'mail_body': fields.text('Mail body', help='Enter the body of the mail when create user affect on this portal'),
        'mail_attachment': fields.binary('Attachment', help='Permit to send attachment (eg, portal user manuel)'),
    }

    #_defaults = {
    #    'company_id': lambda self, cr, uid, c: self.pool.get('res.company')._company_default_get(cr, uid, 'extranet.portal', c)
    #}

    def create_action_menu(self, cr, uid, action_id, action_name, context=None):
        """
        Create default menu for the users of the portal.
        """
        if context is None:
            context = {}

        mod_obj = self.pool.get('ir.model.data')
        result = mod_obj._get_id(cr, uid, 'base', 'view_menu')
        args = {
            'name': ('%s main menu') % action_name,
            'usage': 'menu',
            'type': 'ir.actions.act_window',
            'res_model': 'ir.ui.menu',
            'domain': "[('parent_id','=',"+str(action_id)+")]",
            'view_type': 'tree',
            'view_id': mod_obj.read(cr, uid, result, ['res_id'])['res_id'],
        }
        return self.pool.get('ir.actions.act_window').create(cr,uid, args, context=context)


    def create(self, cr, uid, vals, context=None):
        """
        Create an act_window for this menu, to affect on user when portal is selected
        """
        if not vals.get('menu_action_id') :
            vals['menu_action_id']= self.create_action_menu(cr, uid, vals['menu_id'], vals['name'], context)
        return super(ExtranetPortal, self).create(cr, uid, vals, context=context)

ExtranetPortal()



# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
