# -*- coding: utf-8 -*-
##############################################################################
#
#    extranet module for OpenERP, External access for OpenERP
#    Copyright (C) 2011 SYLEAM (<http://www.syleam.fr/>)
#              Christophe CHAUVET <christophe.chauvet@syleam.fr>
#
#    This file is a part of extranet
#
#    extranet is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    extranet is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import osv
from openerp.osv import fields


class IrActionReportXML(osv.osv):
    """
    Add boolean field to restrict the visibility of the report on share access
    """
    _inherit = 'ir.actions.report.xml'

    _columns = {
        'extranet_visible': fields.boolean('Extranet visibility ?', help='Check this if this report must be visible in extranet mode'),
    }

    _defaults = {
        'extranet_visible': False,
    }

IrActionReportXML()




class IrActionActWindow(osv.osv):
    _inherit = 'ir.actions.act_window'

    _columns = {
        'extranet_visible': fields.boolean('Extranet visibility ?', help='Check this if this report must be visible in extranet mode'),
    }

    _defaults = {
        'extranet_visible': False,
    }

IrActionActWindow()



# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
