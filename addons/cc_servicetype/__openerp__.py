# -*- coding: utf-8 -*-

{
    'name': 'Caption Colorado Service Type',
    'version': '0.1',
    'category': 'Tools',
    'description': """
This module supports managing service types and levels
=================================================================

Use for managing service types and levels

""",
    'author': 'Aspenware',
    'category': 'Caption Colorado',
    'website': 'http://www.aspenware.com',
    'summary': 'Caption Colorado Service Types',
    'depends': [
        'sale',
    ],
    'data': [
        'service_type_view.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
