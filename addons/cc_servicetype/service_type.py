from openerp.osv import osv, fields


class cc_servicetype_serviceline(osv.osv):
    _name = 'cc.servicetype.serviceline'
    _description = 'CC Service Line'
    _order = 'sequence'

    _columns = {
        'name': fields.char('Name'),
        'legacy_id': fields.char('Legacy Id'),
        'sequence': fields.integer('Sequence'),
    }


class cc_servicetype_paycategory(osv.osv):
    _name = 'cc.servicetype.paycategory'
    _description = 'Pay Category'
    _order = 'sequence'

    _columns = {
        'name': fields.char('Type'),
        'legacy_id': fields.char('Legacy Id'),
        'sequence': fields.integer('Sequence'),
    }

class cc_servicetype_servicetype(osv.osv):
    _name = 'cc.servicetype.servicetype'
    _description = 'Service Type'
    _order = 'sequence'

    _columns = {
        'name': fields.char('Type'),
        'service_group_id': fields.many2one('cc.servicetype.servicegroup', 'Service Group'),
        'pay_category_id': fields.many2one('cc.servicetype.paycategory', 'Pay Category'),
        'abbr': fields.char('Abbr'),
        'standard_price': fields.float('StandardPrice'),
        'unit_of_measure': fields.char('Unit-Of-Measure'),
        'minimum_billing_unit': fields.float('Minimum-Billing-Unit'),
        'minimum_fee': fields.float('Minimum-Fee'),
        'minimum_block': fields.char('Minimum-Block'),
        'color': fields.char('Color'),
        'legacy_id': fields.char('Legacy Id'),
        'sequence': fields.integer('Sequence'),
    }

class cc_servicetype_servicegroup(osv.osv):
    _name = 'cc.servicetype.servicegroup'
    _description = 'CC Service Group'
    _order = 'sequence'

    _columns = {
        'name': fields.char('Service Group'),
        'service_line_id': fields.many2one('cc.servicetype.serviceline', 'Service Line'),
        'customer_service_rating_type_ids': fields.one2many('cc.servicetype.customerserviceratingstype', 'service_group_id', 'C Ratings'),
        'employee_service_rating_type_ids': fields.one2many('cc.servicetype.employeeserviceratingstype', 'service_group_id', 'E Ratings'),
        'service_type_ids': fields.one2many('cc.servicetype.servicetype', 'service_group_id', 'Service Types'),
        'legacy_id': fields.char('Legacy Id'),
        'sequence': fields.integer('Sequence'),
    }

class cc_servicetype_customerserviceratingstype(osv.osv):
    _name = 'cc.servicetype.customerserviceratingstype'
    _description = "CC Service Customer Service Ratings Type"
    _order = 'sequence'

    _columns = {
        'name': fields.char('Name'),
        'service_group_id': fields.many2one('cc.servicetype.servicegroup', 'Service Group'),
        'legacy_id': fields.char('Legacy Id'),
        'rating': fields.integer('Rating'),
        'sequence': fields.integer('Sequence'),
    }
    

class cc_servicetype_employeeserviceratingstype(osv.osv):
    _name = 'cc.servicetype.employeeserviceratingstype'
    _description = "CC Service Employee Service Ratings Type"
    _order = 'sequence'

    _columns = {
        'name': fields.char('Name'),
        'service_group_id': fields.many2one('cc.servicetype.servicegroup', 'Service Group'),
        'legacy_id': fields.char('Legacy Id'),
        'rating': fields.integer('Rating'),
        'sequence': fields.integer('Sequence'),
    }
