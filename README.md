Caption Colorado Odoo
---------------------


PostgreSQL Database Dependencies
--------------------------------

Odoo uses the [PostgreSQL database](http://www.postgresql.org/) to store data. PostgreSQL runs natively on [Linux](http://www.postgresql.org/download/), [Macintosh](http://www.postgresql.org/download/macosx/), and [Windows](http://www.postgresql.org/download/windows/).

Once you have installed PostgreSQL on your machine, you will need to [create a database](http://www.postgresql.org/docs/9.0/static/sql-createdatabase.html) and a [database role](http://www.postgresql.org/docs/8.1/static/sql-createrole.html). If you are running windows, we recommend using [PgAdmin III](http://www.pgadmin.org/) to manage your PostgreSQL server. If you are running a Macintosh or a Linux environment, here are some one-liners to copy and paste:

From the command line, run the following commands:

**Assume the postgres user**
        
        sudo su postgres
        
**Create A Database Role**

        createuser openerp # make this new user a super user when prompted

**Start the PostgreSQL Console Under the Standard Template Database** (if not already done)

        psql template1
    
**Give the _openerp_ User A Password**

        template1=# alter role openerp with password 'postgres';
        
**Create a Database for Odoo to Use**

        CREATE DATABASE odoo OWNER openerp

**Finally Grant Privileges***

        GRANT ALL PRIVILEGES ON ALL TABLES to openerp


Starting the Open ERP Server
----------------------------

The Open ERP server runs within an application container called [Werkzeug](http://werkzeug.pocoo.org/). To start the application container running, use the `openerp-server` script file. In order to connect Odoo to your PostgreSQL database, you can either pass commandline arguments or use a configuration file, which is the preferred method. To do this, append the `--config myconfig.conf` flag as a parameter to the script invocation. Here's an example:

```
python ./openerp-server --config server.conf
```

For more information, see the [configuration section of the getting started document](https://doc.odoo.com/trunk/server/01_getting_started/#configuration).


Example Server Configuration File
---------------------------------

        [options]
        pg_path = /usr/bin
        
        db_host = 127.0.0.1
        db_port = 5432
        
        db_user = odoo
        db_name = odoo
        db_password = odoo
        
        addons_path = addons
        data_dir = data
        
        logfile = openerp-server.log

<<<<<<< HEAD



