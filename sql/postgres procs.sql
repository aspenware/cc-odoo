﻿CREATE OR REPLACE FUNCTION resource_add_note(note_id integer, tag_id integer)
  RETURNS void AS
$BODY$
    BEGIN
      INSERT INTO cc_employee_note_tag_rel (note_id, tag_id) VALUES (note_id, tag_id);
    END;
    $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION resource_add_note(integer, integer)
  OWNER TO postgres;


CREATE OR REPLACE FUNCTION add_note(note_id integer, tag_id integer)
  RETURNS void AS
$BODY$
    BEGIN
      INSERT INTO cc_customer_note_tag_rel (note_id, tag_id) VALUES (note_id, tag_id);
    END;
    $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION add_note(integer, integer)
  OWNER TO postgres;


CREATE OR REPLACE FUNCTION add_job_type(employee_id integer, job_type_id integer)
  RETURNS void AS
$BODY$
    BEGIN
      INSERT INTO cc_hr_job_type_rel (employee_id, job_type_id) VALUES (employee_id, job_type_id);
    END;
    $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION add_job_type(integer, integer)
  OWNER TO postgres;
